

resource "aws_iam_role" "external_dns" {
  name               = format("%s-external-dns", var.resource_name)
  description        = format("%s externalDNS role", var.resource_name)
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Principal": {
        "Federated": "${var.oidc_provider_arn}"
      },
      "Effect": "Allow",
      "Condition": {
        "StringEquals": {
          "${var.oidc_url}:sub": "system:serviceaccount:${local.namespace}:${var.sa_name}"
        }
      }
    }
  ]
}
EOF

  tags = {
    "ServiceAccountName"      = var.sa_name
    "ServiceAccountNameSpace" = local.namespace
  }
}

resource "aws_iam_role_policy" "external_dns" {
  name   = format("%s-external-dns", var.resource_name)
  role   = aws_iam_role.external_dns.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "route53:ChangeResourceRecordSets"
      ],
      "Resource": [
        "arn:aws:route53:::hostedzone/${var.zone_id}"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ListHostedZones",
        "route53:ListResourceRecordSets"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "kubernetes_namespace" "external_dns" {
  count = local.create_ns
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_service_account" "external_dns" {
  metadata {
    name      = var.sa_name
    namespace = local.namespace
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.external_dns.arn
    }
  }
  automount_service_account_token = true
}

resource "kubernetes_cluster_role" "external_dns" {
  metadata {
    name = "external-dns"
  }

  rule {
    api_groups = [""]
    resources  = ["services", "endpoints", "pods"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["list", "watch"]
  }

  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "external_dns_viewer" {
  metadata {
    name = "external-dns-viewer"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.external_dns.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.external_dns.metadata.0.name
    namespace = kubernetes_service_account.external_dns.metadata.0.namespace
  }
}

resource "kubernetes_deployment" "external_dns" {
  metadata {
    name      = "external-dns"
    namespace = local.namespace
    labels = {
      "app" = "external-dns"
    }
  }

  spec {
    strategy {
      type = "Recreate"
    }

    selector {
      match_labels = {
        "app" = "external-dns"
      }
    }
    template {
      metadata {
        labels = {
          "app" = "external-dns"
        }

        annotations = {
          "eks.amazonaws.com/role-arn" = aws_iam_role.external_dns.arn
        }
      }
      spec {
        automount_service_account_token = true
        service_account_name            = kubernetes_service_account.external_dns.metadata.0.name
        container {
          name  = "external-dns"
          image = var.container_image
          args = [
            "--source=ingress",
            "--source=service",
            "--provider=aws",
            "--aws-zone-type=public", # only look at public hosted zones (valid values are public, private or no value for both)
            format("--domain-filter=%s", var.hosted_zone), # will make ExternalDNS see only the hosted zones matching provided domain, omit to process all available hosted zones
            "--interval=30s",
            "--policy=upsert-only",   # would prevent ExternalDNS from deleting any records, omit to enable full synchronization
            "--registry=txt",
            format("--txt-owner-id=%s", var.zone_id),
          ]
        }
        security_context {
          fs_group = 65534
        }
      }
    }
  }
}
