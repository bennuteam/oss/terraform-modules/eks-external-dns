variable "container_image" {
  type    = string
  default = "registry.opensource.zalan.do/teapot/external-dns:v0.7.0"
}
variable "hosted_zone" { type = string }
variable "sa_name" {
  type    = string
  default = "external-dns"
}

variable "namespace" {
  type    = string
  default = "kube-system"
}
variable "oidc_provider_arn" { type = string }
variable "oidc_url" { type = string }
variable "resource_name" { type = string }
variable "zone_id" {
  type    = string
  default = "*"
}
