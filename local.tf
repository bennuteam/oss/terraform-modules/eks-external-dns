locals {
  create_ns = var.namespace != "kube-system" ? 1 : 0
  namespace = local.create_ns == 1 ? kubernetes_namespace.external_dns.0.metadata.0.name : var.namespace
}
